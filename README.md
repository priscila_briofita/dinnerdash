# README

**Observações**

As controllers do projeto foram criadas seguindo um mesmo padrão tanto para action quanto para suas respectivas rotas.
Sendo assim:

    * O método index da classe Meals pode ser acessado através de uma requisição GET na rota /meals
        -> Caso seja passado um atributo '**search**' na requisição, o método filtra refeições por **Nome** ou **Descrição** de acordo com o valor do atributo
        -> Caso não seja passado o atributo, o método retorna todos os dados de Meals

    * A mesma lógica se aplica a classe Meal_category, que também faz uso da gema 'ransack' no método index caso seja passado um atributo '**search**'
    
**Busca por usuários**
    Por questões de maior praticidade, foi usado como parâmetro o nome do Usuário nas rotas referentes a **Atualização, Exclusão e Busca de um usuário específico**
    
    Seguindo o seguinte padrão, variando somente os métodos POST, GET e DELETE:
    
    * <dominio>/users/[:Nome do usuário]
    