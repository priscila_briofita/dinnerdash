# frozen_string_literal: true

class AuthenticationsController < ApplicationController
  require "#{Rails.root}/lib/JsonWebToken"

  def login
    user = User.find_by(email: params[:email])
    if user&.authenticate(params[:password])
      auth_token = JsonWebToken.encode({ user_id: user.id })
      render json: { auth_token: auth_token, username: user.name }, status: :ok
    else
      render json: { error: 'Login Unsuccessfull' }, status: :unauthorized
    end
  end
end
