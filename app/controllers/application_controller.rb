# frozen_string_literal: true

class ApplicationController < ActionController::API
  protected

  def authenticate_request!
    return invalid_authentication if !payload || !JsonWebToken.valid_payload(payload.first)

    load_current_user!
    invalid_authentication unless @current_user
  end

  def invalid_authentication
    render json: { error: 'Invalid Request' }, status: :unauthorized
  end

  private

  def payload
    auth_header = request.headers['Authorization']
    token = auth_header.split(' ').last
    JsonWebToken.decode(token)
  rescue StandardError
    nil
  end

  def load_current_user!
    @current_user = User.find_by(id: payload[0]['user_id'])
  end
end
