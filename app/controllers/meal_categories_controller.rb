# frozen_string_literal: true

class MealCategoriesController < ApplicationController
  before_action :set_meal_category, only: %i[show update destroy]

  def index
    @q = MealCategory.ransack(name_cont: params[:search])
    @mealcategories = @q.result(distinct: true)
    render json: @mealcategories, status: 200
  end

  def show
    render json: @mealcategory, status: 200
  end

  def create
    @mealcategory = MealCategory.new(meal_category_params)
    if @mealcategory.save
      render json: @mealcategory, status: 201
    else
      render json: @mealcategory.errors, status: :unprocessable_entity
    end
  end

  def update
    if @mealcategory.update(meal_category_params)
      render json: @mealcategory, status: 200
    else
      render json: @mealcategory.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @mealcategory.destroy
  end

  private

  def set_meal_category
    @mealcategory = MealCategory.find(params[:id])
  end

  def meal_category_params
    params.permit(:name)
  end
end
