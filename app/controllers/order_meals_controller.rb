# frozen_string_literal: true

class OrderMealsController < ApplicationController
  before_action :set_order_meal, only: %i[show update destroy]

  # GET "/ordermeals"
  def index
    @ordermeals = OrderMeal.all
    render json: @ordermeals, status: 200
  end

  # GET "/ordermeals/:id"
  def show
    render json: @ordermeal, status: 200
  end

  # POST "/ordermeals"
  def create
    @ordermeal = OrderMeal.new(order_meal_params)
    if @ordermeal.save
      render json: @ordermeal, status: 201
    else
      render json: @ordermeal.errors, status: :unprocessable_entity
    end
  end

  # PUT "/ordermeals/:id"
  def update
    if @ordermeal.update(order_meal_params)
      render json: @ordermeal, status: 200
    else
      render json: @ordermeal, status: :unprocessable_entity
    end
  end

  # DELETE "/ordermeals/:id"
  def destroy
    @ordermeal.destroy
  end

  private

  def set_order_meal
    @ordermeal = OrderMeal.find(params[:id])
  end

  def order_meal_params
    params.permit(:quantity, :meal_id, :order_id)
  end
end
