# frozen_string_literal: true

class OrdersController < ApplicationController
  before_action :set_order, only: %i[show update destroy]
  # GET "/orders"
  def index
    @orders = Order.all
    render json: @orders, status: 200
  end

  # GET "/orders/:id"
  def show
    render json: @order, status: 200
  end

  # POST "/orders"
  def create
    @order = Order.new(order_params)
    if @order.save
      render json: @order, status: 201
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # PUT/PATCH "/orders/:id"
  def update
    if @order.update(order_params)
      render json: @order, status: 200
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # DELETE "/orders/:id"
  def destroy
    @order.destroy
  end

  private

  def set_order
    @order = Order.find(params[:id])
  end

  def order_params
    params.permit(:price, :situation_id, :user_id)
  end
end
