# frozen_string_literal: true

class MealsController < ApplicationController
  before_action :set_meal, only: %i[show update destroy]

  def index
    @q = Meal.ransack(name_cont: params[:search], description_cont: params[:search], m: 'or')
    @meals = @q.result(distinct: true)
    render json: @meals, status: 200
  end

  def show
    if @meal.image.attached?
      render json:@meal.as_json.merge({ image: url_for(@meal.image) })
    else
      render json: @meal, status: 200
    end
  end
  def create
    @meal = Meal.new(meal_params)
    if @meal.save
      @url_image = url_for(@meal.image)
      render json: @meal, status: 201
    else
      render json: @meal.errors, status: :unprocessable_entity
    end
  end

  def update
    if @meal.update(meal_params)
      render json: @meal, status: 200
    else
      render json: @meal.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @meal.destroy
  end

  private

  def set_meal
    @meal = Meal.find(params[:id])
  end

  def meal_params
    params.permit(:name, :description, :available, :price, :image, :meal_category_id)
  end
end
