# frozen_string_literal: true

class OrderMeal < ApplicationRecord
  validates :quantity, presence: true
  validates :quantity, numericality: { message: 'somente números' }

  before_save :price_calculate
  before_destroy :destroy_meal

  has_many :meals
  has_many :orders

  belongs_to :meal
  belongs_to :order

  private
  def price_calculate
    meal = Meal.find(meal_id)
    order = Order.find(order_id)
    price = (meal.price * quantity) + self.order.price
    order.update_attribute(:price, price)
  end

  def destroy_meal
    meal = Meal.find(meal_id)
    order = Order.find(order_id)
    price = self.order.price - (meal.price * quantity)
    order.update_attribute(:price, price)
  end
  
end
