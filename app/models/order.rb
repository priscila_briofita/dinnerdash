# frozen_string_literal: true

class Order < ApplicationRecord
  
  before_create :set_initial_price

  has_many :order_meals, dependent: :destroy
  belongs_to :situation
  belongs_to :user

  private

  def set_initial_price
    self.price = 0
  end
end
