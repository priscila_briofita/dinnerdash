# frozen_string_literal: true

class MealCategory < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: { message: 'deve ser único' }

  has_many :meals

  before_destroy :update_inexistent_category

  def update_inexistent_category
    meals = Meal.where(meal_category_id: id)
    category = MealCategory.find_by_name('Sem Categoria')
    category = MealCategory.create(name: 'Sem Categoria') if category.nil?
    meals.each do |meal|
      meal.update_attribute(:meal_category_id, category.id)
    end
  end
end
