# frozen_string_literal: true

class Meal < ApplicationRecord
  has_one_attached :image

  validates :name, presence: true
  validates :description, length: { maximum: 500 }
  validates :price, presence: true 

  belongs_to :meal_category
  has_many :order_meals
end
