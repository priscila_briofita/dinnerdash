# frozen_string_literal: true

class User < ApplicationRecord
  validates :name, presence: true
  validates :email, presence: true
  validates :password, presence: true

  validates_uniqueness_of :name
  validates_uniqueness_of :email, case_sensitive: false
  validates_format_of :email, with: /@/

  validates :name, length: { maximum: 50 }
  validates :email, length: { maximum: 60 }
  validates :password, length: { maximum: 8 }

  has_secure_password

  before_destroy :delete_relation

  has_many :orders, dependent: :destroy

  def delete_relation
    username = User.find_by_name(name)
    Order.where(user_id: username.id).destroy_all
  end
end
