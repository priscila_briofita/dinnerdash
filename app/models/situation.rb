# frozen_string_literal: true

class Situation < ApplicationRecord
  validates :description, presence: true
  validates :description, length: { maximum: 200 }

  has_many :orders
end
